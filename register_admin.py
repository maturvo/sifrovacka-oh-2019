from getpass import getpass
from werkzeug.security import generate_password_hash
from app.models import User
from app import db

name = 'septima'
kidak = False
admin = True

c = True
while c:
    c = False
    password = getpass("Heslo: ")
    p2 = getpass("Zopakuj heslo: ")

    if password != p2:
        print("Hesla sa nezhoduju")
        c = True

u = User(name=name,password=generate_password_hash(password, "sha256"),kidak=kidak, admin=admin)
db.session.add(u)
db.session.commit()
