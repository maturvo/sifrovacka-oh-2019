from flask_wtf import FlaskForm
from wtforms import SelectField, PasswordField, BooleanField, SubmitField, StringField, IntegerField, TextField, FileField, DateField
from wtforms.validators import DataRequired


class LoginForm(FlaskForm):
    username = StringField('Class', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember = BooleanField('Remember me', default=True)
    submit = SubmitField('Sign In')


class ChangePasswordForm(FlaskForm):
    password = PasswordField('New password', validators=[DataRequired()])
    password2 = PasswordField('Confirm new password', validators=[DataRequired()])
    submit = SubmitField('Change password')


class SubmitForm(FlaskForm):
    solution = StringField('Riešenie:', validators=[DataRequired()])
    submit = SubmitField('Submit')


class HintForm(FlaskForm):
    hint = SubmitField('Použi hint')


class SkipForm(FlaskForm):
    skip = SubmitField('Preskoč')


class RegistrationForm(FlaskForm):
    username = StringField('Class', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField('Confirm password', validators=[DataRequired()])
    kidak = BooleanField('Kidak')
    submit = SubmitField('Register')


class AddSifraForm(FlaskForm):
    heslo = StringField('Heslo', validators=[DataRequired()])
    submit = SubmitField('Add')


class AddSifra3Form(AddSifraForm):
    hint = TextField('Hint')
    location = TextField('Location')
    skipable = BooleanField('Skipable')
    hintable = BooleanField('Hintable')

class AddRoundForm(FlaskForm):
    zadania = FileField('Zadania')
    hinty = FileField('Hinty')
    vzoraky = FileField('Vzoraky')
    start = DateField('Zaciatok kola', format='%d.%m.%Y')
    submit = SubmitField('Create')


class AddMessage(FlaskForm):
    title = StringField('Title')
    content = TextField('Content')
    kidaci = BooleanField('2.stupen')
    submit = SubmitField('Submit')


class AddQuestion(FlaskForm):
    title = StringField('Title')
    text = TextField('Text otázky')
    submit = SubmitField('Submit')


class AnswerQuestion(FlaskForm):
    answer = TextField('Odpoveď')
    public = BooleanField('Public')
    submit = SubmitField('Submit')
