from app import app
from flask import request, render_template, flash, redirect, send_file
from flask_login import current_user, login_user, logout_user, login_required

from .models import User, Sifra, Submit, BadSubmit, Kolo, Game, Message, Question
from .forms import LoginForm, SubmitForm, HintForm, ChangePasswordForm, SkipForm, RegistrationForm, AddSifraForm, AddSifra3Form, AddRoundForm, AddMessage, AddQuestion, AnswerQuestion

from werkzeug.security import check_password_hash, generate_password_hash
from app import db

import datetime
import unidecode


MAX_SUBMITS = 5
COOLDOWN = datetime.timedelta(minutes=5)
ROUND_START = datetime.timedelta(hours=8)
HINTS_AFTER = datetime.timedelta(days=7)
ROUND_DURATION = datetime.timedelta(days=14)

@app.context_processor
def inject():
    m2 = messages(current_user, True)
    m3 = messages(current_user, False)
    if not current_user.is_authenticated:
        return dict(new_message2=False, new_message3=False)
    return dict(new_message2=(len(m2) > 0 and m2[0][1].date_time > (current_user.checked_messages2 if current_user.admin else current_user.checked_messages)), new_message3=(len(m3) > 0 and m3[0][1].date_time > current_user.checked_messages))

def game_started():
    return Game.query.first().start < datetime.datetime.now()

def game_finished():
    return Game.query.first().end < datetime.datetime.now()

def can_submit():
    s = BadSubmit.query.filter_by(user=current_user).order_by('date_time').all()
    print([ss.date_time for ss in s])
    if len(s) == 0:
        return datetime.timedelta()
    if s[-1].date_time + COOLDOWN < datetime.datetime.now():
        current_user.bad_submits.clear()
        for sub in s:
            db.session.delete(sub)
        db.session.commit()
    s = BadSubmit.query.filter_by(user=current_user).order_by('date_time').all()
    if len(s) < MAX_SUBMITS:
        return datetime.timedelta()
    return (s[-1].date_time + COOLDOWN) - datetime.datetime.now()



@app.route('/')
@app.route('/index')
def index():
    if current_user.is_authenticated and current_user.admin:
        return render_template("layout_a.html")
    return render_template("index.html")

@app.route('/2stupen')
def st2():
    kola = Kolo.query.order_by('cislo').all()
    print([r.start.strftime("(Začína %d.%m.)") if r.start + ROUND_START > datetime.datetime.now() else "" for r in kola])
    return render_template("layout2.html", kola=kola, roundinfo=[r.start.strftime("(Začína %d.%m.)") if r.start + ROUND_START > datetime.datetime.now() else "" for r in kola])

@app.route('/3stupen')
def st3():
    return render_template("layout3.html", game_finished=game_finished(), game_started=game_started(), game_end=Game.query.first().end)

@app.route('/triedy')
def triedy():
    if not current_user.admin:
        return redirect('/')
    return render_template('triedy.html', users = User.query.all())

@app.route('/delete_user/<int:id>')
def delete_user(id):
    if not current_user.admin:
        return redirect('/')
    u = User.query.get(id)
    db.session.delete(u)
    db.session.commit()
    return redirect('/triedy')

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    classes = [u.name for u in User.query.all()]
    form.username.choices = classes
    if form.validate_on_submit():
        user = User.query.filter_by(name=form.username.data).first()
        if user is None or not check_password_hash(user.password, form.password.data):
            flash("Invalid username or password", 'danger')
            return redirect('/login')
        login_user(user, remember = form.remember.data)
        flash("Login succesful", 'info')
        if user.admin:
            return redirect('/sifry')
        return redirect('/2stupen') if user.kidak else redirect('/3stupen')
    return render_template('login.html', title='Sign In', form=form, classes=classes)

@app.route('/logout')
@login_required
def logout():
    logout_user()
    flash("You have been logged out", 'info')
    return redirect('/')

@app.route('/change_password', methods=['GET', 'POST'])
@login_required
def change_password():
    form = ChangePasswordForm()
    if form.validate_on_submit():
        if form.password.data != form.password2.data:
            flash("Passwords do not match", 'danger')
            return redirect('/change_password')
        current_user.password = generate_password_hash(form.password.data, method="sha256")
        db.session.commit()
        flash("Password changed succesfuly", 'info')
        return redirect('/2.stupen') if current_user.kidak else redirect('/3stupen')
    return render_template('change_password.html', form=form)

def hfix(heslo):
    return unidecode.unidecode(heslo).lower()

@app.route('/3stupen/submit', methods=['GET', 'POST'])
@login_required
def submit():
    if current_user.kidak:
        flash("Táto stránka je len pre 3. stupen", 'danger')
        return redirect('/3stupen')
    if not game_started():
        flash("Hra ešte nezačala", 'danger')
        return redirect('/3stupen')
    if game_finished():
        flash("Hra skončila", 'danger')
        return redirect('/3stupen')
    submit_form = SubmitForm()
    hint_form = HintForm()
    skip_form = SkipForm()
    sifra = Sifra.query.filter_by(kidaci=False, napovedna=False, cislo=current_user.solved_normalne+1).first()
    if not sifra:
        flash("Už si vyriešil všetky šifry", 'info')
        return redirect('/3stupen')
    print(request.form)
    if submit_form.validate_on_submit():
        cs = can_submit()
        if cs > datetime.timedelta():
            flash("Odovzdáš príliš často. Znova môžeš odovzdávať o " + cs.__str__(), 'danger')
            return redirect('/3stupen/submit')
        if hfix(submit_form.solution.data) != sifra.heslo:
            s = BadSubmit(date_time=datetime.datetime.now())
            current_user.bad_submits.append(s)
            sifra.bad_submits.append(s)
            db.session.commit()
            flash("Nesprávne heslo", 'danger')
            return redirect('/3stupen/submit')
        current_user.solved_normalne += 1
        s = Submit(date_time=datetime.datetime.now(), hint=current_user.hint_used)
        current_user.hint_used = False
        current_user.submits.append(s)
        sifra.submits.append(s)
        db.session.commit()
        flash("Heslo ok", 'success')
        return redirect('/3stupen/submit')
    if hint_form.validate_on_submit() and 'hint' in request.form:
        if current_user.hints == 0:
            flash("Žiadne hinty v zásobe", 'danger')
            return redirect('/3stupen/submit')
        flash("Hint bol použitý", 'info')
        current_user.hints -= 1
        current_user.hint_used = True
        db.session.commit()
        return redirect('/3stupen/submit')
    if skip_form.validate_on_submit() and 'skip' in request.form:
        cost = 1 if current_user.hint_used else 2
        if current_user.hints < cost:
            flash("Nemáš dostatočný počet hintov", 'danger')
            return redirect('/3stupen/submit')
        flash("Preskočil si šifru", 'info')
        current_user.hints -= cost
        current_user.solved_normalne += 1
        current_user.hint_used = False
        s = Submit(date_time=datetime.datetime.now(), skip=True)
        current_user.submits.append(s)
        sifra.submits.append(s)
        db.session.commit()
        db.session.commit()
        return redirect('/3stupen/submit')
    return render_template("submit.html", skip_form=skip_form, submit_form=submit_form, hint_form=hint_form, sifra=sifra, hinted=current_user.hint_used,
                           game_finished=game_finished(), game_started=game_started(), game_end=Game.query.first().end)

@app.route('/3stupen/submit_napovedne')
@login_required
def submit_napovedne_view():
    if current_user.kidak:
        flash("Táto stranka je len pre 3. stupen", 'danger')
        return redirect('/')
    if not game_started():
        flash("Hra ešte nezačala", 'danger')
        return redirect('/3stupen')
    if game_finished():
        flash("Hra skončila", 'danger')
        return redirect('/3stupen')
    form = SubmitForm()
    solved = [Submit.query.filter_by(sifra=s, user=current_user).count() > 0 for s in Sifra.query.filter_by(kidaci=False, napovedna=True).order_by('cislo').all()]
    return render_template("submit_napovedne.html", form=form, solved=solved, n=Sifra.query.filter_by(kidaci=False, napovedna=True).count(),
                           game_finished=game_finished(), game_started = game_started(), game_end = Game.query.first().end)

@app.route('/3stupen/submit_napovedne/<int:sifra_num>', methods=['POST'])
@login_required
def submit_napovedne(sifra_num):
    if current_user.kidak:
        flash("Táto stranka je len pre 3. stupen", 'danger')
        return redirect('/3stupen')
    if not game_started():
        flash("Hra ešte nezačala", 'danger')
        return redirect('/3stupen')
    if game_finished():
        flash("Hra skončila", 'danger')
        return redirect('/3stupen')
    form = SubmitForm()
    if form.validate_on_submit():
        cs = can_submit()
        if cs > datetime.timedelta():
            flash("Odovzdáš príliš často. Znova môžeš odovzdávať o " + cs.__str__(), 'danger')
            return redirect('/3stupen/submit_napovedne')
        sifra = Sifra.query.filter_by(kidaci=False, napovedna=True, cislo=sifra_num).first()
        if Submit.query.filter_by(sifra=sifra, user=current_user).count() > 0:
            flash("Túto šifru si už odovzdal", 'danger')
            return redirect('/3stupen/submit_napovedne')
        if hfix(form.solution.data) != sifra.heslo:
            s = BadSubmit(date_time=datetime.datetime.now())
            current_user.bad_submits.append(s)
            sifra.bad_submits.append(s)
            db.session.commit()
            flash("Nesprávne heslo", 'danger')
            return redirect('/3stupen/submit_napovedne')
        current_user.hints += 1
        s = Submit(date_time=datetime.datetime.now())
        current_user.submits.append(s)
        sifra.submits.append(s)
        db.session.commit()
        flash("Heslo ok", 'success')
    return redirect('/3stupen/submit_napovedne')

@app.route('/2stupen/kolo_<int:k>')
@login_required
def submit2_view(k):
    if not current_user.kidak:
        flash("Táto stranka je len pre 2. stupen", 'danger')
        return redirect('/')
    kolo = Kolo.query.filter_by(cislo=k).first()
    form = SubmitForm()
    n = Sifra.query.filter_by(kidaci=True, kolo=kolo).count()
    solved = [Submit.query.filter_by(sifra=s, user=current_user).count() > 0 for s in kolo.sifry]
    kola = Kolo.query.order_by('cislo').all()
    return render_template("submit2.html", form=form, solved=solved, n=n, kola=kola,
                           kolo=kolo, roundinfo=[r.start.strftime("(Začína %d.%m.)") if r.start + ROUND_START > datetime.datetime.now() else "" for r in kola],
                           show_content=kolo.start + ROUND_START < datetime.datetime.now(),
                           show_hints=(kolo.start + ROUND_START + HINTS_AFTER < datetime.datetime.now()),
                           start=kolo.start.strftime("%d.%m."), hints=(kolo.start + HINTS_AFTER).strftime("%d.%m."), finished=(datetime.datetime.now() > kolo.start + ROUND_DURATION + ROUND_START))

@app.route('/2stupen/kolo_<int:k>/<int:sifra_num>', methods=['POST'])
@login_required
def submit2(k, sifra_num):
    kolo = Kolo.query.filter_by(cislo=k).first()
    if not current_user.kidak:
        flash("Táto stranka je len pre 2. stupen", 'danger')
        return redirect('/')
    if kolo.start > datetime.datetime.now():
        flash("V tomto kole sa ešte nedá odovzdávať", 'danger')
        return redirect('/')
    if datetime.datetime.now() > kolo.start + ROUND_DURATION:
        flash("V tomto kole sa už nedá odovzdávať", 'danger')
        return redirect('/')
    form = SubmitForm()
    if form.validate_on_submit():
        sifra = Sifra.query.filter_by(kidaci=True, kolo=kolo, cislo=sifra_num).first()
        if Submit.query.filter_by(sifra=sifra, user=current_user).count() > 0:
            flash("Túto šifru si už odovzdal", 'danger')
            return redirect('/2stupen/kolo_{}'.format(k))
        if hfix(form.solution.data) != sifra.heslo:
            s = BadSubmit(date_time=datetime.datetime.now())
            current_user.bad_submits.append(s)
            sifra.bad_submits.append(s)
            db.session.commit()
            flash("Nesprávne heslo", 'danger')
            return redirect('/2stupen/kolo_{}'.format(k))
        s = Submit(date_time=datetime.datetime.now(), hint=(kolo.start + ROUND_START + HINTS_AFTER < datetime.datetime.now()))
        current_user.submits.append(s)
        sifra.submits.append(s)
        db.session.commit()
        flash("Heslo ok", 'success')
    return redirect('/2stupen/kolo_{}'.format(k))

def submits(u, sifry):
    return [Submit.query.filter_by(user=u, sifra=s).first() for s in sifry]

@app.route('/3stupen/vysledky')
def vysledky3():
    uu = User.query.filter_by(kidak=False, admin=False).all()
    users = []
    usubmits = {}
    sif = Sifra.query.filter_by(kidaci=False, napovedna=False)
    for u in uu:
        usubmits[u] = submits(u, sif)
        subs = [s.date_time if s else None for s in usubmits[u]]
        score = (sum(x is not None for x in subs), max(s if s else datetime.datetime.min for s in subs))
        users.append((score, u))
        print(subs)
        print(score)
    users.sort(key=lambda x: (-x[0][0], x[0][1]))
    s = current_user.solved_normalne if current_user.is_authenticated else 0
    if game_finished() or (current_user.is_authenticated and current_user.admin):
        s = sif.count()
    return render_template('vysledky3.html', users=enumerate([x[1] for x in users]), submits=usubmits, s=s,
                           game_finished=game_finished(), game_started=game_started(), game_end=Game.query.first().end)

@app.route('/2stupen/vysledky')
def vysledky2():
    uu = User.query.filter_by(kidak=True, admin=False).all()
    kola = []
    for k in Kolo.query.order_by('cislo').all():
        if k.start + ROUND_START < datetime.datetime.now():
            kola.append(k)
    users = []
    round_scores = {}
    all_scores = {}
    snames = []
    for k in kola:
        for i, s in enumerate(k.sifry):
            snames.append("{}.{}".format(k.cislo, i + 1))
    for u in uu:
        score = [0, 0, 0]
        all_scores[u] = []
        round_scores[u] = []
        for k in kola:
            kscore = [0, 0, 0]
            for s in k.sifry:
                sscore = [0,0,0]
                sub = Submit.query.filter_by(user=u, sifra=s).first()
                if not sub:
                    all_scores[u].append(sscore)
                    continue
                kscore[0] += 1
                kscore[1] += sub.hint
                kscore[2] += (sub.date_time - sub.sifra.kolo.start).days
                kscore[2] += BadSubmit.query.filter_by(user=u, sifra=s).count()

                sscore[0] += 1
                sscore[1] += sub.hint
                sscore[2] += (sub.date_time - sub.sifra.kolo.start).days
                sscore[2] += BadSubmit.query.filter_by(user=u, sifra=s).count()
                all_scores[u].append(sscore)
            for i in range(3):
                score[i] += kscore[i]
            round_scores[u].append(kscore)
        users.append((score, u))
        print(score)
    users.sort(key=lambda x: (-x[0][0], x[0][1], x[0][2]))
    kolall = Kolo.query.order_by('cislo').all()
    print([round_scores[u] for i,u in users])
    if current_user.is_authenticated and current_user.admin:
        return render_template('vysledky2_admin.html', users=enumerate(users), round_scores=all_scores, sifry=snames, kola=kolall, roundinfo=[r.start.strftime("(Začína %d.%m.)") if r.start + ROUND_START > datetime.datetime.now() else "" for r in kolall])
    return render_template('vysledky2.html', users=enumerate(users), round_scores=round_scores, kol=len(kola), kola=kolall, roundinfo=[r.start.strftime("(Začína %d.%m.)") if r.start + ROUND_START > datetime.datetime.now() else "" for r in kolall])

@app.route('/add_user', methods=['GET', 'POST'])
@login_required
def add_user():
    if not current_user.admin:
        return redirect('/')
    form = RegistrationForm()
    if form.validate_on_submit():
        if form.password.data != form.password2.data:
            flash("Passwords do not match", 'danger')
            return redirect('/add_user')
        u = User(name=form.username.data, password=generate_password_hash(form.password.data, "sha256"),kidak=form.kidak.data)
        db.session.add(u)
        db.session.commit()
        flash("Registration succesful", 'info')
        return redirect('/add_user')
    return render_template('add_user.html', form=form)

def make_rounds(k):
    if Kolo.query.filter_by(cislo=k).first():
        return
    if k == 0:
        return
    kolo = Kolo(cislo=k)
    db.session.add(kolo)
    db.session.commit()
    make_rounds(k-1)

@app.route('/pridaj_sifru', methods=['GET', 'POST'])
@login_required
def pridaj_sifru():
    if not current_user.admin:
        return redirect('/')
    form = AddSifra3Form()
    if form.validate_on_submit():
        s = Sifra(cislo=Sifra.query.filter_by(kidaci=False,napovedna=False).count()+1, kidaci=False, napovedna=False, heslo=hfix(form.heslo.data))
        s.hint = form.hint.data
        s.location = form.location.data
        s.skipable = form.skipable.data
        s.hintable = form.hintable.data
        db.session.add(s)
        db.session.commit()
        flash("Sifra pridana", 'info')
        return redirect('/sifry')
    return render_template('add_sifra.html', form=form)

@app.route('/pridaj_sifru_n', methods=['GET', 'POST'])
@login_required
def pridaj_sifru_n():
    if not current_user.admin:
        return redirect('/')
    form = AddSifraForm()
    if form.validate_on_submit():
        s = Sifra(cislo=Sifra.query.filter_by(kidaci=False,napovedna=True).count()+1, kidaci=False, napovedna=True, heslo=hfix(form.heslo.data))
        db.session.add(s)
        db.session.commit()
        flash("Sifra pridana", 'info')
        return redirect('/sifry')
    return render_template('add_sifra_n.html', form=form)

@app.route('/pridaj_sifru_k/<int:kolo>', methods=['GET', 'POST'])
@login_required
def pridaj_sifru_k(kolo):
    if not current_user.admin:
        return redirect('/')
    form = AddSifraForm()
    if form.validate_on_submit():
        k = Kolo.query.filter_by(cislo=kolo).first()
        s = Sifra(cislo=Sifra.query.filter_by(kidaci=True,napovedna=False,kolo=k).count()+1, kidaci=True, napovedna=False, heslo=hfix(form.heslo.data))
        k.sifry.append(s)
        db.session.add(s)
        db.session.commit()
        flash("Sifra pridana", 'info')
        return redirect('/sifry')
    return render_template('add_sifra_n.html', form=form)

def file_extension(filename):
    return filename.rsplit('.', 1)[1].lower()

@app.route('/pridaj_kolo', methods=['GET', 'POST'])
@login_required
def pridaj_kolo():
    if not current_user.admin:
        return redirect('/')
    form=AddRoundForm()
    if form.validate_on_submit():
        k = Kolo(cislo=Kolo.query.count()+1, start=form.start.data)
        if form.zadania.data.filename == "" or form.hinty.data.filename == "" or form.vzoraky.data.filename == "":
            flash("Pridaj subory", 'danger')
            return redirect('/pridaj_kolo')
        print(file_extension(form.zadania.data.filename) != 'pdf',file_extension(form.hinty.data.filename))
        if file_extension(form.zadania.data.filename) != 'pdf' or file_extension(form.hinty.data.filename) != 'pdf' or file_extension(form.hinty.data.filename) != 'pdf':
            flash("Zly subor", 'danger')
            return redirect('/pridaj_kolo')
        form.zadania.data.save('app/uploads/zadania{}.pdf'.format(k.cislo))
        form.hinty.data.save('app/uploads/hinty{}.pdf'.format(k.cislo))
        form.hinty.data.save('app/uploads/vzoraky{}.pdf'.format(k.cislo))
        print(form.zadania.data.filename, form.hinty.data)
        db.session.add(k)
        db.session.commit()
        return redirect('/sifry')
    return render_template('add_round.html', form=AddRoundForm())

@app.route('/uprav_kolo/<int:k>', methods=['GET', 'POST'])
@login_required
def uprav_kolo(k):
    if not current_user.admin:
        return redirect('/')
    form=AddRoundForm()
    if form.validate_on_submit():
        k = Kolo.query.filter_by(cislo=k).first()
        if form.zadania.data.filename != "":
            if file_extension(form.zadania.data.filename) != 'pdf':
                flash("Zly subor", 'danger')
                return redirect('/uprav_kolo{}'.format(k))
            form.zadania.data.save('app/uploads/zadania{}.pdf'.format(k.cislo))
        if form.hinty.data.filename != "":
            if file_extension(form.hinty.data.filename) != 'pdf':
                flash("Zly subor", 'danger')
                return redirect('/uprav_kolo{}'.format(k))
            form.hinty.data.save('app/uploads/hinty{}.pdf'.format(k.cislo))
        if form.vzoraky.data.filename != "":
            if file_extension(form.vzoraky.data.filename) != 'pdf':
                flash("Zly subor", 'danger')
                return redirect('/uprav_kolo{}'.format(k))
            form.vzoraky.data.save('app/uploads/vzoraky{}.pdf'.format(k.cislo))
        k.start = form.start.data
        db.session.commit()
        return redirect('/sifry')
    return render_template('add_round.html', form=AddRoundForm())

@app.route('/odstran_kolo')
@login_required
def odstran_kolo():
    if not current_user.admin:
        return redirect('/')
    k = Kolo.query.order_by('cislo').all()[-1]
    if len(k.sifry) > 0:
        flash("Kolo ma sifry", 'danger')
        return redirect('/sifry')
    db.session.delete(k)
    db.session.commit()
    return redirect('/sifry')

@app.route('/odstran_sifru')
@login_required
def odstran_sifru():
    if not current_user.admin:
        return redirect('/')
    s = Sifra.query.filter_by(kidaci=False, napovedna=False).order_by('cislo').all()[-1]
    db.session.delete(s)
    db.session.commit()
    return redirect('/sifry')

@app.route('/odstran_sifru_n')
@login_required
def odstran_sifru_n():
    if not current_user.admin:
        return redirect('/')
    s = Sifra.query.filter_by(kidaci=False, napovedna=True).order_by('cislo').all()[-1]
    db.session.delete(s)
    db.session.commit()
    return redirect('/sifry')

@app.route('/odstran_sifru_k/<int:kolo>')
@login_required
def odstran_sifru_k(kolo):
    if not current_user.admin:
        return redirect('/')
    s = Sifra.query.filter_by(kidaci=True, kolo=Kolo.query.filter_by(cislo=kolo).first()).order_by('cislo').all()[-1]
    db.session.delete(s)
    db.session.commit()
    return redirect('/sifry')

@app.route('/sifry')
@login_required
def sifry():
    if not current_user.admin:
        return redirect('/')
    normalne = Sifra.query.filter_by(kidaci=False, napovedna=False).order_by('cislo').all()
    napovedne = Sifra.query.filter_by(kidaci=False, napovedna=True).order_by('cislo').all()
    kola=Kolo.query.order_by('cislo').all()
    kidacke = [k.sifry for k in kola]
    zaciatok = [k.start.strftime("%d.%m") for k in kola]
    for k in kidacke:
        k.sort(key=lambda x: x.cislo)
    return render_template('sifry.html', normalne=normalne, napovedne=napovedne, kidacke=enumerate(kidacke), zaciatok=zaciatok)

@app.route('/hinty<int:kolo>')
def hinty(kolo):
    k = Kolo.query.filter_by(cislo=kolo).first()
    if k.start + ROUND_START + HINTS_AFTER > datetime.datetime.now():
        flash("Hinty ešte nie sú dostupné", 'danger')
        return redirect('2stupen')
    return send_file('uploads/hinty{}.pdf'.format(kolo), as_attachment=True)

@app.route('/zadania<int:kolo>')
def zadania(kolo):
    k = Kolo.query.filter_by(cislo=kolo).first()
    if k.start + ROUND_START > datetime.datetime.now():
        flash("Zadania ešte nie sú dostupné", 'danger')
        return redirect('2stupen')
    return send_file('uploads/zadania{}.pdf'.format(kolo), as_attachment=True)

@app.route('/vzoraky<int:kolo>')
def vzoraky(kolo):
    k = Kolo.query.filter_by(cislo=kolo).first()
    if k.start + ROUND_START + ROUND_DURATION > datetime.datetime.now():
        flash("Vzoráky ešte nie sú dostupné", 'danger')
        return redirect('2stupen')
    return send_file('uploads/vzoraky{}.pdf'.format(kolo), as_attachment=True)


def messages(user, kidaci):
    m = [(False, m) for m in Message.query.filter_by(kidaci=kidaci).all()] + [(True, q) for q in Question.query.filter_by(kidaci=kidaci, public=True)]
    if user.is_authenticated:
        if user.admin:
            m += [(True, q) for q in Question.query.filter_by(kidaci=kidaci, public=False).all()]
        else:
            m += [(True, q) for q in Question.query.filter_by(kidaci=kidaci, public=False, user=user).all()]
    if user.is_authenticated and user.admin:
        m.sort(key=lambda x: (x[0] and x[1].answer == "", x[1].date_time))
    else:
        m.sort(key=lambda x: x[1].date_time)
    m.reverse()
    return m


@app.route('/remove_message<int:id>')
@login_required
def rmm(id):
    if not current_user.admin:
        return redirect('/')
    m = Message.query.get(id)
    st = 2 if m.kidaci else 3
    db.session.delete(m)
    db.session.commit()
    return redirect('/{}stupen/spravy'.format(st))

@app.route('/2stupen/spravy')
def spravy2():
    if current_user.is_authenticated:
        if current_user.admin:
            lc = current_user.checked_messages2
            current_user.checked_messages2 = datetime.datetime.now()
        else:
            lc = current_user.checked_messages
            current_user.checked_messages = datetime.datetime.now()
    else:
        lc = datetime.datetime.now()
    db.session.commit()
    m = messages(current_user, True)
    kola = Kolo.query.order_by('cislo').all()
    return render_template('spravy2.html', messages=m, lc=lc, kola=kola, roundinfo=[r.start.strftime("(Začína %d.%m.)") if r.start + ROUND_START > datetime.datetime.now() else "" for r in kola])

@app.route('/3stupen/spravy')
def spravy3():
    if current_user.is_authenticated:
        lc = current_user.checked_messages
    else:
        lc = datetime.datetime.now()
    current_user.checked_messages = datetime.datetime.now()
    db.session.commit()
    m = messages(current_user, False)
    return render_template('spravy3.html', messages=m, lc=lc)

@app.route('/2stupen/otazka', methods=['GET', 'POST'])
@login_required
def otazka2():
    if not current_user.kidak:
        flash("Musíš byť 2. stupeň aby si sa tu mohol pýtať otázky.", 'danger')
        return redirect('/2stupen/spravy')
    form = AddQuestion()
    if form.validate_on_submit():
        q = Question(title=form.title.data, question=form.text.data, date_time=datetime.datetime.now(), kidaci=True)
        current_user.questions.append(q)
        db.session.add(q)
        db.session.commit()
        flash("Otázka pridaná", 'info')
        return redirect('/2stupen/spravy')
    return render_template('add_question.html', form=form)

@app.route('/3stupen/otazka', methods=['GET', 'POST'])
@login_required
def otazka3():
    if current_user.kidak:
        flash("Musíš byť 3. stupeň aby si sa tu mohol pýtať otázky.", 'danger')
        return redirect('/3stupen/spravy')
    form = AddQuestion()
    if form.validate_on_submit():
        q = Question(title=form.title.data, question=form.text.data, date_time=datetime.datetime.now(), kidaci=False)
        current_user.questions.append(q)
        db.session.add(q)
        db.session.commit()
        flash("Otázka pridaná", 'info')
        return redirect('/3stupen/spravy')
    return render_template('add_question.html', form=form)

@app.route('/2stupen/sprava', methods=['GET', 'POST'])
@login_required
def sprava2():
    if not current_user.admin:
        return redirect('/2stupen/spravy')
    form = AddMessage()
    if form.validate_on_submit():
        m = Message(title=form.title.data, content=form.content.data, date_time=datetime.datetime.now(), kidaci=True)
        db.session.add(m)
        db.session.commit()
        flash("Správa pridaná", 'info')
        return redirect('/2stupen/spravy')
    return render_template('add_message.html', form=form)

@app.route('/3stupen/sprava', methods=['GET', 'POST'])
@login_required
def sprava3():
    if not current_user.admin:
        return redirect('/3stupen/spravy')
    form = AddMessage()
    if form.validate_on_submit():
        m = Message(title=form.title.data, content=form.content.data, date_time=datetime.datetime.now(), kidaci=False)
        db.session.add(m)
        db.session.commit()
        flash("Správa pridaná", 'info')
        return redirect('/3stupen/spravy')
    return render_template('add_message.html', form=form)

@app.route('/odpoved/<int:id>', methods=['GET', 'POST'])
@login_required
def odpoved(id):
    if not current_user.admin:
        return redirect('/')
    form = AnswerQuestion()
    question = Question.query.get(id)
    if form.validate_on_submit():
        question.date_time = datetime.datetime.now()
        question.answer = form.answer.data
        print(form.answer.data)
        question.public = form.public.data
        db.session.commit()
        flash("Odpoveď pridaná", 'info')
        return redirect('/{}stupen/spravy'.format(2 if question.kidaci else 3))
    return render_template('answer_question.html', form=form, question=question)
