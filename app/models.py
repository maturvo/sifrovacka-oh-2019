from flask_login import UserMixin
from . import db
from datetime import datetime

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    password = db.Column(db.String(100))

    kidak = db.Column(db.Boolean, default=False)

    submits = db.relationship('Submit', backref='user', lazy=True)
    bad_submits = db.relationship('BadSubmit', backref='user', lazy=True)
    questions = db.relationship('Question', backref='user', lazy=True)

    #velki
    hints = db.Column(db.Integer, default=0)
    solved_normalne = db.Column(db.Integer, default=0)
    hint_used = db.Column(db.Boolean, default=False)

    admin = db.Column(db.Boolean, default=False)

    checked_messages = db.Column(db.DateTime, default=datetime.min)
    checked_messages2 = db.Column(db.DateTime, default=datetime.min) #for admin


class Sifra(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    cislo = db.Column(db.Integer)
    kidaci = db.Column(db.Boolean, default=False)
    heslo = db.Column(db.String(100))

    napovedna = db.Column(db.Boolean, default=False)
    hint = db.Column(db.Text, default="")
    location = db.Column(db.Text, default="")
    skipable = db.Column(db.Boolean, default=True)
    hintable = db.Column(db.Boolean, default=True)

    submits = db.relationship('Submit', backref='sifra', lazy=True)
    bad_submits = db.relationship('BadSubmit', backref='sifra', lazy=True)

    kolo_id = db.Column(db.Integer, db.ForeignKey('kolo.id'))


class Kolo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    cislo = db.Column(db.Integer)
    start = db.Column(db.DateTime)

    sifry = db.relationship('Sifra', backref='kolo', lazy=True)

class Submit(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sifra_id = db.Column(db.Integer, db.ForeignKey('sifra.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    date_time = db.Column(db.DateTime)
    
    skip = db.Column(db.Boolean, default=False)
    hint = db.Column(db.Boolean, default=False)


class BadSubmit(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    sifra_id = db.Column(db.Integer, db.ForeignKey('sifra.id'))
    date_time = db.Column(db.DateTime)

class Game(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    start = db.Column(db.DateTime)
    end = db.Column(db.DateTime)


class Message(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String)
    content = db.Column(db.Text)
    date_time = db.Column(db.DateTime)
    kidaci = db.Column(db.Boolean)


class Question(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String)
    question = db.Column(db.Text)
    answer = db.Column(db.Text, default="")
    public = db.Column(db.Boolean, default=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    date_time = db.Column(db.DateTime)
    kidaci = db.Column(db.Boolean)
